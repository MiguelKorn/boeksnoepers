/**
 * Responsible for handling the actions happening on welcome view
 *
 * @author Miguel Korn
 */
function welcomeController() {
    var welcomeView;

    function initialize() {
        $.get("views/welcome.html")
            .done(setup)
            .fail(error);
    }

    //Called when the profile-detail.html has been loaded
    function setup(data) {
        //Load the profile detail content into memory
        welcomeView = $(data);

        welcomeView.find('.login-btn').on('click', function(){
            welcomeView.find('.modal').modal('show');
        });

        welcomeView.find('.login-content button').on('click', logUserIn);

        //Empty the content-div and add the resulting view to the page
        $(".content").empty().append(welcomeView);
    }

    function logUserIn(e) {
        var username = welcomeView.find('#username').val();
        var password = welcomeView.find('#password').val();

        apiManager.get('login', false, true, {"username":username, "password":password})
            .done(function (response) {
                if(response.success) {
                    var user = response.data;
                    localStorage.setItem('user_id', user.id);
                    localStorage.setItem('token', user.api_token);
                    localStorage.setItem('fname', user.first_name);
                    welcomeView.find('.modal').modal('hide');
                    if(user.teacher === null) localStorage.setItem('teacher', 1);
                    loadController('books');
                } else {
                    welcomeView.find('.login-content .error')
                        .html('<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>Helaas!</strong> Er ging iets mis, probeer opnieuw!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                }
            })
            .fail(function (error) {
                console.log("error", error);
            });
    }

    //Called when the login.html failed to load
    function error() {
        $(".content").html("Failed to load content!");
    }

    //Run the initialize function to kick things off
    initialize();
}