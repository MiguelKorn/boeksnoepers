/**
 * Responsible for handling the actions happening on the nav
 *
 * @author Miguel Korn
 */
function navigationController() {
    //Reference to our loaded view
    var navigationView;

    function initialize() {
        $.get("views/navigation.html")
            .done(setup)
            .fail(error);
    }

    //Called when the login.html has been loaded
    function setup(data) {
        //Load the login-content into memory
        navigationView = $(data);

        updateNavigation();

        navigationView.find("a").on("click", handleClick);

        //Empty the content-div and add the resulting view to the page
        $(".nav").empty().append(navigationView);
    }

    function updateNavigation() {
        if (localStorage.getItem('user_id') !== null) {
            navigationView.find('.navbar-header').removeClass('hide');

            if(localStorage.getItem('teacher') === null){
                navigationView.find('span.name').html(localStorage.getItem('fname'));
            } else {
                navigationView.find('.navbar-header a:nth-child(2)').addClass('hide');
            }
        } else {
            navigationView.find('.navbar-header').addClass('hide');

        }
    }

    function handleClick(e) {
        var controller = $(this).attr("data-controller");
        loadController(controller);
        return false;
    }

    //Called when the login.html failed to load
    function error() {
        $(".content").html("Failed to load content!");
    }

    //Run the initialize function to kick things off
    initialize();
}