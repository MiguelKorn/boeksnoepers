/**
 * Responsible for handling the actions happening on profile view
 *
 * @author Miguel Korn
 */
function profileController() {
    var profileView;
    var scoreboardItemView;
    var bookItemView;
    var user_id = parseInt(localStorage.getItem('user_id'));

    function initialize() {
        var profileRequest = $.get("views/profile.html");
        var scoreboardItemRequest = $.get("views/templates/scoreboard-item.html");
        var bookItemRequest = $.get("views/templates/book-item.html");

        $.when(profileRequest, scoreboardItemRequest, bookItemRequest)
            .done(function (profileResponse, scoreboardItemResponse, bookItemResponse) {
                setup(profileResponse[0], scoreboardItemResponse[0], bookItemResponse[0]);
            })
            .fail(error);
    }

    //Called when the profile-detail.html has been loaded
    function setup(profileResponse, scoreboardItemResponse, bookItemResponse) {
        //Load the profile detail content into memory
        profileView = $(profileResponse);
        scoreboardItemView = $(scoreboardItemResponse);
        bookItemView = $(bookItemResponse);

        apiManager.get("users/" + user_id)
            .done(function (data) {
                displayUserInfo(data[0]);
            })
            .fail(function (error) {
                console.log("error", error);
            });

        apiManager.get("competitions/current")
            .done(displayScoreboard)
            .fail(function (error) {
                console.log("error", error);
            });

        apiManager.get("users/" + user_id + "/books")
            .done(displayBooks)
            .fail(function (error) {
                console.log("error", error);
            });

        profileView.find('.info').on('click', function () {
            profileView.find('.modal').modal("show");
        });

        //Empty the content-div and add the resulting view to the page
        $(".content").empty().append(profileView);
    }

    function displayUserInfo(user) {
        var fullname = getFullname(user.first_name, user.last_name_prefix, user.last_name);
        var teacher = user.teacher;
        var bgColor = (user.gender === "M") ? (isOdd(user.id - 1) ? "blue" : "green") : (isOdd(user.id) ? "purple" : "pink");

        if (user.highest_place !== null) {
            profileView.find('.profile-picture .highest_place img')
                .attr('src', 'assets/img/badges/ribbon_' + user.highest_place + '.png')
                .attr('alt', 'ribbon_'+user.highest_place);;
        } else {
            profileView.find('.profile-picture .highest_place img').remove();
        }

        profileView.addClass(bgColor);
        profileView.find('.profile-picture .image-container img')
            .attr('src', 'assets/img/users/' + user.image)
            .attr('alt', fullname);
        profileView.find('.profile-info .name').html(fullname);
        profileView.find('.profile-info .group').html(user.group);
        profileView.find('.profile-info .teacher').html((teacher.gender === "M" ? "Meester " : "Juf ") + teacher.first_name);
    }

    function displayScoreboard(users) {
        var userScores = [];

        $.each(users, function (index, user) {
            var totalScore = 0;

            $.each(user.books, function (key, book) {
                if (!book.is_current) {
                    totalScore += parseInt(book.score);
                }
            });

            userScores.push({
                id: user.id,
                score: totalScore,
                fullname: getFullname(user.first_name, user.last_name_prefix, user.last_name)
            });
        });

        userScores.sort(function (u1, u2) {
            if (u1.score < u2.score) return 1;
            if (u1.score > u2.score) return -1;
            return 0;
        });

        $.each(userScores, function (index, user) {
            var scoreboardItem = scoreboardItemView.clone();

            if (user.id === user_id) {
                scoreboardItem.addClass('table-success');
            }

            scoreboardItem.find('.number').html("#" + (index + 1));
            scoreboardItem.find('.name').html(user.fullname);
            scoreboardItem.find('.score').html(user.score);

            profileView.find('.scoreboard table tbody').append(scoreboardItem);
        });
        
        apiManager.get("competitions/3")
            .done(function (data) {
                // calculate days left to end of competition
                var daysLeft = Math.ceil((new Date(new Date(data.end_date) - new Date()))/1000/60/60/24);
                profileView.find('.scoreboard .days-left').html("Nog " + daysLeft + " dag(en)");
            })
            .fail(function (error) {
                console.log("error", error);
            });
    }
    
    function displayBooks(books) {
        var count=0;
        $.each(books, function(index, book){
            var user = (book.user.length !== 0) ? book.user[0] : false;

            var bookItem = bookItemView.clone();

            bookItem.addClass(getBackgroundColor(book.id));
            bookItem.find("img.book")
                .attr("src", (book.image.length > 9) ? book.image : "assets/img/books/" + book.image)
                .attr("alt", book.title);
            bookItem.find(".header h2").first().html(book.title);
            bookItem.find(".info").first().html(book.description);
            bookItem.find("button").attr("value", book.id);
            bookItem.find(".modal").remove();

            if (user) {
                if (user.is_current) {
                    bookItem.find('button.btn').removeClass("btn-primary")
                        .addClass("btn-success")
                        .html("QUIZ")
                        .attr("data-type", "quiz");
                } else {
                    bookItem.find('.overlay')
                        .removeClass('hide');
                    bookItem.find('.overlay span').html(user.score);
                    bookItem.find('.overlay img')
                        .attr('src', 'assets/img/badges/badge_' + user.score + '.png')
                        .attr('alt', 'badge score:' + user.score);
                    bookItem.find("button.btn")
                        .html("GELEZEN")
                        .removeClass("btn-success")
                        .addClass("btn-secondary read")
                        .prop("disabled", true);
                }
            }

            bookItem.on("click", function (e) {
                var target = $(e.target);
                if (target.is("button")) {
                    if (target.html() === "LEZEN") {
                        apiManager.get('users/currentBook?user_id=' + user_id + '&new_book_id=' + target.val(), false, true)
                            .done(function (data) {
                                if (data.success) {
                                    $(profileView).find(".modal").modal("hide");
                                    loadController("books");
                                }
                            });
                    } else if (target.html() === "QUIZ") {
                        $(profileView).find(".modal").modal("hide");
                        loadController("quiz", book)
                    }
                }
            });

            if((user && !user.is_current)) {
                profileView.find(".read-books-container .books").append(bookItem);
            } else {
                if(count < 2) {
                    profileView.find(".new-books-container .books").append(bookItem);
                    count++;
                }
            }
        });

        profileView.find('.all-books').on('click', function(){
            loadController('books');
        });
    }

    //Called when the login.html failed to load
    function error() {
        $(".content").html("Failed to load content!");
    }

    //Run the initialize function to kick things off
    initialize();
}