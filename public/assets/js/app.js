//Global variables
var session = sessionManager();
var apiManager = apiManager();

//Constants (sort of)
var CONTROLLER_NAVIGATION = "navigation";
var CONTROLLER_WELCOME = "welcome";
var CONTROLLER_BOOKS = "books";
var CONTROLLER_QUIZ = "quiz";
var CONTROLLER_PROFILE = "profile";
var CONTROLLER_LOGOUT = "logout";

//This is called when the browser is done loading
$(function () {
    //Always load the navigation
    loadController(CONTROLLER_NAVIGATION);

    //Attempt to load the controller from the URL, if it fails, fall back to the books controller.
    loadControllerFromUrl(CONTROLLER_WELCOME);

    //Setup the database manager
    // apiManager.connect("https://boeksnoepers-api.herokuapp.com/api/");
    // apiManager.authenticate("yourtokenhere");
});

//This function is responsible for creating the controllers of all views
function loadController(name, controllerData) {
    console.log("loadController: " + name);

    if (controllerData) {
        console.log(controllerData);
    } else {
        controllerData = {};
    }

    switch (name) {
        case CONTROLLER_NAVIGATION:
            navigationController();
            break;
        case CONTROLLER_WELCOME:
            setCurrentController(name);
            navigationController();
            welcomeController();
            break;
        case CONTROLLER_BOOKS:
            setCurrentController(name);
            navigationController();
            isLoggedIn(bookListController, welcomeController);
            break;
        case CONTROLLER_QUIZ:
            setCurrentController(name);
            navigationController();
            hasCurrentBook(function(){
                quizController(controllerData);
            }, bookListController);
            break;
        case CONTROLLER_PROFILE:
            setCurrentController(name);
            navigationController();
            isLoggedIn(profileController, welcomeController);
            break;
        case CONTROLLER_LOGOUT:
            setCurrentController(name);
            handleLogout();
            navigationController();
            break;
        default:
            return false;
    }

    return true;
}

function loadControllerFromUrl(fallbackController) {
    var currentController = getCurrentController();

    if (currentController) {
        if (!loadController(currentController)) {
            loadController(fallbackController);
        }
    } else {
        loadController(fallbackController);
    }
}

function getCurrentController() {
    return location.hash.slice(1);
}

function setCurrentController(name) {
    location.hash = name;
}

//Convenience functions to handle logged-in states
function isLoggedIn(whenYes, whenNo) {
    if(localStorage.getItem("user_id")) {
        whenYes();
    }
    else {
        whenNo();
    }
}

function hasCurrentBook(whenYes, whenNo) {
    apiManager.get('users/' + parseInt(localStorage.getItem('user_id')) + '/currentBook')
        .done(function (data) {
            (data.length !== 0) ? whenYes() : whenNo();
        })
        .fail(function (error) {
            console.log(error);
            whenNo();
        });
}

function getFullname(firstName, lastNamePrefix, lastName) {
    return firstName + (!!lastNamePrefix ? " " + lastNamePrefix + " " : " ") + lastName;
}

function isOdd(num) {
    return num % 2;
}

function getBackgroundColor(number) {
    return (number % 4 === 1) ? 'purple' :
        (number % 4 === 2) ? 'green' :
            (number % 4 === 3) ? 'pink' : 'blue';
}

function handleLogout() {
    localStorage.clear();
    loadController(CONTROLLER_WELCOME);
}