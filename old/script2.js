$(function () {
    $(".modal.boek-1").modal('show');

    $("button").on("click", function (e) {
        // console.log(e.target.value);
        $(this).addClass("huidig");
        $("button").not(this).each(function () {
            $(this)
                .addClass('btn btn-secondary')
                .prop("disabled", true);
        });

        openModal(e.target.value);
    })
});

function openModal(value) {
    $(".book-list").addClass("hide");
    $(".book-chosen").removeClass("hide");
    $(".book-"+value).removeClass("hide");
}