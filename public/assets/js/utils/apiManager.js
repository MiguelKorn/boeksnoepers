/**
 * Implementation of a Api Manager
 *
 * @author Miguel Korn
 */
function apiManager() {
    var url = "https://boeksnoepers-api.herokuapp.com/api/";
    var token;

    function connect(newUrl) {
        this.url = newUrl;
    }

    function setAuthentication(newToken) {
        this.token = newToken;
    }

    function get(route, authenticate = false, post = false, data = false) {
        var promise = $.Deferred();

        var headers = {
            "Accept": "application/json"
        };

        var settings = {
            url: url + route,
            type: post ? "POST" : "GET",
            dataType: "json",
            crossDomain: true,
            headers: headers
        };

        if (token && authenticate) headers.Authorization = "Bearer " + token;
        if (data) settings.data = data;

        $.ajax(settings).done(function (data) {
            promise.resolve(data);
        }).fail(function (xhr) {
            var data = JSON.parse(xhr.responseText);

            if (data.error) {
                promise.reject(data.error);
            } else {
                promise.reject("Something bad happened, see console.");
            }
        });

        return promise;
    }

    return {
        connect: connect,
        authenticate: setAuthentication,
        get: get
    }
}