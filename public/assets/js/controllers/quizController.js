/**
 * Responsible for handling the actions happening on quiz view
 *
 * @author Miguel Korn
 */
function quizController(book) {
    var quizView;
    var currentQuestion = 1;
    var score = 0;
    var timer;
    var timeCount = 10;
    var totalTime = 10;

    function initialize() {
        if ($.isEmptyObject(book)) loadController('books');

        $.get("views/quiz.html")
            .done(setup)
            .fail(error);
    }

    function setup(data) {
        quizView = $(data);
        quizView.find(".modal-backdrop").addClass("hide");
        quizView.find(".header h3").html("Quiz - " + book.title);
        quizView.find(".quiz-img img")
            .attr("src", (book.image.length > 9)?book.image:"assets/img/books/" + book.image)
            .attr("alt", book.title);
        quizView.find(".counter").html(10);
        quizView.find(".progress-bar").css("width", "0%");
        setQuestion();


        quizView.find("#start").on("click", function () {
            startQuiz();
        });

        quizView.find(".choice").on("click", function (e) {
            checkAnswer(e.target.id);
        });

        quizView.find("#profile").on("click", function () {
            loadController("profile");
        });

        quizView.find("#books").on("click", function () {
            loadController("books");
        });

        $(".content").empty().append(quizView);
    }

    function setQuestion() {
        var q = book.questions[currentQuestion - 1];
        quizView.find(".question h3").html(q.title);
        quizView.find("#A").html(q.answer_a);
        quizView.find("#B").html(q.answer_b);
        quizView.find("#C").html(q.answer_c);
    }

    function startQuiz() {
        quizView.find(".start").addClass("hide");

        timer = setInterval(changeCounter, 1000);
    }

    function changeCounter() {
        if (timeCount >= 0) {
            quizView.find(".counter").html(timeCount);
            quizView.find(".progress-bar").css("width", ((totalTime - timeCount) * 10) + "%");
            timeCount--;
        } else {
            timeCount = totalTime;
            answerIswrong();
            if (currentQuestion < book.questions.length) {
                currentQuestion++;
                setQuestion();
            } else {
                clearInterval(timer);
                showScore();
            }
        }
    }

    function checkAnswer(answer) {
        if (answer === book.questions[currentQuestion - 1].correct_answer) {
            score++;
            answerIsCorrect();
        } else {
            answerIswrong();
        }

        timeCount = totalTime;
        if (currentQuestion < book.questions.length) {
            currentQuestion++;
            setQuestion();
        } else {
            clearInterval(timer);
            showScore();
        }
    }

    function answerIswrong() {
        quizView.find(".quiz-progress .prog:nth-child(" + currentQuestion + ")").css("background-color", "#DC3545");
    }

    function answerIsCorrect() {
        quizView.find(".quiz-progress .prog:nth-child(" + currentQuestion + ")").css("background-color", "#28A745");
    }

    function showScore() {
        var scorePercentage = Math.round(100 * score / 3);
        var image = (scorePercentage >= 80) ? "5.png" : (scorePercentage >= 60) ? "4.png" : (scorePercentage >= 40) ? "3.png" : (scorePercentage >= 20) ? "2.png" : "1.png";
        var text = (scorePercentage >= 50) ? "Goedzo!" : "Helaas!";
        var points = (scorePercentage === 100) ? 5 : (scorePercentage === 67) ? 3 : Math.floor(scorePercentage / 20) + 1;

        quizView.find(".score img").attr("src", "assets/img/score/" + image);
        quizView.find(".score span:first-child").html(text);
        quizView.find(".score span.percentage").html(scorePercentage + "%");
        quizView.find(".score span.points").html(points);

        apiManager.get('users/currentBook?user_id=' + parseInt(localStorage.getItem('user_id')) + '&score=' + points, false, true)
            .done(function (data) {
                if (data.success) quizView.find(".score").removeClass("hide");
            });
    }

    //Called when the login.html failed to load
    function error() {
        $(".content").html("Failed to load the sidebar!");
    }

    //Run the initialize function to kick things off
    initialize();
}