/**
 * Responsible for handling the actions happening on book list view
 *
 * @author Miguel Korn
 */
function bookListController() {
    //Reference to our loaded view
    var bookListView;
    var bookItemView;
    var locationItemView;
    var user_id = parseInt(localStorage.getItem('user_id'));
    var isTeacher = localStorage.getItem('teacher') !== null;
    var booksData;

    function initialize() {
        var bookListRequest = $.get("views/book-list.html");
        var bookItemRequest = $.get("views/templates/book-item.html");
        var locationItemRequest = $.get("views/templates/location-item.html");

        //Wait for all data to arrive
        $.when(bookListRequest, bookItemRequest, locationItemRequest)
            .done(function (bookListResponse, bookItemResponse, locationItemResponse) {
                //Index 0 of a response is the actual data
                setup(bookListResponse[0], bookItemResponse[0], locationItemResponse[0]);
            })
            .fail(error);
    }

    //Called when the welcome.html has been loaded
    function setup(bookListResponse, bookItemResponse, locationItemResponse) {
        //Load the content into memory
        bookListView = $(bookListResponse);
        bookItemView = $(bookItemResponse);
        locationItemView = $(locationItemResponse);

        //Now load the actual data we want to display
        loadBooks(isTeacher);
        if (isTeacher) bookListView.find('form').submit(handleQuestionsSubmit);
    }

    function loadBooks(isTeacher) {
        apiManager.get("users/" + user_id + "/books")
            .done(function (books) {
                if (isTeacher) {
                    $.get("data/books.json")
                        .done(function (data) {
                            booksData = data;
                            var notAddedBooks;
                            notAddedBooks = data.books.filter(function (book, index) {
                                var indexes = [0, 13, 28, 124, 147, 280, 269, 297];
                                return indexes.includes(index);
                            });

                            notAddedBooks = notAddedBooks.filter(function (book) {
                                return !books.map(function(book){return book.id}).includes(book.id);
                            });

                            displayBooks(books, notAddedBooks);
                        })
                        .fail(error);
                } else {
                    displayBooks(books);
                }
            })
            .fail(error);
    }

    function displayBooks(books, notAddedBooks) {
        //Loop through the data
        var allBooks = isTeacher ? $.merge(books, notAddedBooks) : books;
        $.each(allBooks, function (index, book) {
            if (!isTeacher) {
                var user = (book.user.length !== 0) ? book.user[0] : false;
            } else {
                var isNotReadBook = notAddedBooks.map(function(b){return b.id}).includes(book.id);
            }
            var bookItem = bookItemView.clone();

            bookItem.addClass(getBackgroundColor(book.id));
            bookItem.find("img.book")
                .attr("src", (book.coverImg) ? book.coverImg : (book.image.length > 9) ? book.image : "assets/img/books/" + book.image)
                .attr("alt", book.title);
            bookItem.find(".header h2").first().html(book.title);
            bookItem.find(".info").first().html(book.description);
            bookItem.find("button").attr("value", book.id);

            if (user) {
                if (user.is_current) {
                    bookItem.find('button.btn').removeClass("btn-primary")
                        .addClass("btn-success")
                        .html("QUIZ")
                        .attr("data-type", "quiz");
                } else {
                    bookItem.find('.overlay')
                        .removeClass('hide');
                    bookItem.find('.overlay span').html(user.score);
                    bookItem.find('.overlay img')
                        .attr('src', 'assets/img/badges/badge_' + user.score + '.png')
                        .attr('alt', 'badge score:' + user.score);
                    bookItem.find("button.btn")
                        .html("GELEZEN")
                        .removeClass("btn-success")
                        .addClass("btn-secondary read")
                        .prop("disabled", true);
                }
            }

            if (!isTeacher) {
                setAvailableLocations(book.locations, bookItem);
                bookItem.on("click", function (e) {
                    var target = $(e.target);
                    if (target.is("button")) {
                        if (target.html() === "LEZEN") {
                            apiManager.get('users/currentBook?user_id=' + user_id + '&new_book_id=' + target.val(), false, true)
                                .done(function (data) {
                                    if (data.success) {
                                        $(bookListView).find(".modal").modal("hide");
                                        displayAvailableModal(book.id, book.locations);
                                        changeButtons(book.id);
                                    }
                                });
                        } else if (target.html() === "QUIZ") {
                            $(bookListView).find(".modal").modal("hide");
                            loadController("quiz", book)
                        }
                    } else {
                        $(this).find(".modal").modal("show");
                    }
                });
            } else {
                if(isNotReadBook) {
                    bookItem.find('button')
                        .html('Toevoegen')
                        .on('click', function (e) {
                            $('input[name="book-id"]').attr('value', book.id);
                            $('input[name="book-title"]').attr('value', book.title);
                            $('input[name="book-desc"]').attr('value', book.body);
                            $('input[name="book-img"]').attr('value', book.coverImg);
                            bookListView.find('.modal.questions').modal('show');
                        });
                } else {
                    bookItem.find('button')
                        .html('Toegevoegd')
                        .addClass("btn-secondary")
                        .prop("disabled", true);
                }
            }

            //Add the profile tile to the view
            bookListView.find(".book-container").append(bookItem);
        });

        //Empty the content-div and add the resulting view to the page
        $(".content").empty().append(bookListView);
    }

    function changeButtons(id) {
        bookListView.find("button.btn").not(".read")
            .addClass("btn-primary")
            .removeClass("btn-success")
            .html("LEZEN")
            .attr("data-type", "lezen");

        bookListView.find("button.btn[value='" + id + "']").not(".read")
            .removeClass("btn-primary")
            .addClass("btn-success")
            .html("QUIZ")
            .attr("data-type", "quiz");
    }

    function setAvailableLocations(locations, bookItem = false) {
        $.each(locations, function (index, location) {
            var locationItem = locationItemView.clone();
            var is_available = location.pivot.is_available;

            locationItem.find('i').addClass(is_available ? 'fa-check-circle' : 'fa-times-circle');
            locationItem.find('span').html(location.name);
            if (bookItem) {
                bookItem.find('.available-locations ul').append(locationItem);
            } else {
                bookListView.find('.modal.available ul').append(locationItem)
            }
        });
    }

    function displayAvailableModal(id, locations) {
        bookListView.find('.modal.available').addClass(getBackgroundColor(id));
        setAvailableLocations(locations);

        bookListView.find('.modal.available').modal("show");
    }

    function handleQuestionsSubmit(e) {
        e.preventDefault();
        //book => title, desc, img
        //question => book_id, order, title, correct_answer, answer_a, answer_b, answer_c

        var book = {
            id: $('input[name="book-id"]').val(),
            title: $('input[name="book-title"]').val(),
            desc: $('input[name="book-desc"]').val(),
            img: $('input[name="book-img"]').val()
        };

        var questions = [
            {
                id: book.id + 1,
                book_id: book.id,
                title: $('input[name="question1"]').val(),
                answer_a: $('input[name="answer1a"]').val(),
                answer_b: $('input[name="answer1b"]').val(),
                answer_c: $('input[name="answer1c"]').val(),
                correct_answer: $('input[name="correctAnswer1"]:checked').val(),
                order: 1
            },
            {
                id: book.id + 2,
                book_id: book.id,
                title: $('input[name="question2"]').val(),
                answer_a: $('input[name="answer2a"]').val(),
                answer_b: $('input[name="answer2b"]').val(),
                answer_c: $('input[name="answer2c"]').val(),
                correct_answer: $('input[name="correctAnswer2"]:checked').val(),
                order: 2
            },
            {
                id: book.id + 3,
                book_id: book.id,
                title: $('input[name="question3"]').val(),
                answer_a: $('input[name="answer3a"]').val(),
                answer_b: $('input[name="answer3b"]').val(),
                answer_c: $('input[name="answer3c"]').val(),
                correct_answer: $('input[name="correctAnswer3"]:checked').val(),
                order: 3
            }
        ];

        apiManager.get('books', false, true, book)
            .done(function (data) {
                if (data.success) {
                    var locations = [];
                    $.each(booksData.availability[book.id], function (index, location) {
                        locations.push({
                            id: book.id + index,
                            book_id: book.id,
                            location_id: index + 1,
                            is_available: location.available
                        });
                    });

                    apiManager.get('locations', false, true, {locations: JSON.stringify(locations)})
                        .done(function (data) {
                            if (data.success) apiManager.get('questions', false, true, {questions: JSON.stringify(questions)})
                                .done(function (done) {
                                    if (done.success) {
                                        $('.modal.questions').modal('hide');
                                        setTimeout(function () {
                                            loadController('books');
                                        }, 500);
                                    }
                                }).fail(function (error) {
                                    console.log("error", error);
                                })
                        })
                        .fail(function (error) {
                            console.log("error", error);
                        })
                }
            })
            .fail(function (error) {
                console.log("error", error);
            })
    }

//Called when the login.html failed to load
    function error() {
        $(".content").html("Failed to load content!");
    }

//Run the initialize function to kick things off
    initialize();
}