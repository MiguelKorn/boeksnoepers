$(function () {
    var startBtn = $('#start');
    var doneBtn = $('#done');
    var stopBtn = $('#stop');
    var radioBtns = $('input[name=rating]');
    var rateBookPage = $('.rateBookPage');
    var doneReadingPage = $('.doneReadingPage');
    var prevBtn = $('#prev');
    var nextBtn = $('#next');
    var progressBar = $('.progress-bar');

    startBtn.on("click", function () {
        startBtn.addClass('hide');
        doneBtn.removeClass('hide');
        stopBtn.removeClass('hide');
    });

    stopBtn.on("click", function () {
        startBtn.removeClass('hide');
        doneBtn.addClass('hide');
        stopBtn.addClass('hide');
    });

    radioBtns.on("click", function () {
        nextBtn.removeClass('disabled');
    });

    nextBtn.on("click", function () {
        if(!nextBtn.hasClass('disabled')) {
            rateBookPage.addClass('hide');
            doneReadingPage.removeClass('hide');
            progressBar.css('width', '100%');
            prevBtn.removeAttr('data-dismiss');
            setTimeout(function () {
                nextBtn.attr('data-dismiss', 'modal');
            }, 500);
        }
    });
    
    prevBtn.on("click", function (e) {
        e.preventDefault();
        if(prevBtn.attr('data-dismiss') !== 'modal') {
            rateBookPage.removeClass('hide');
            doneReadingPage.addClass('hide');
            progressBar.css('width', '0');
            setTimeout(function () {
                prevBtn.attr('data-dismiss', 'modal');
            }, 500);
        } else {
            console.log(prevBtn.attr('data-dismiss'));
            console.log('dismissed');
        }
    })
});